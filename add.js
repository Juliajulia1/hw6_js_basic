// Практичні завдання
// 1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної ціни товару з урахуванням знижки. Викличте цей метод та результат виведіть в консоль.



let product = {
    name: 'Dyson',
    price: 25000,
    discount: 0.05,
};
function countSale() {
    let salePrice = product["price"]*product["discount"];
    let summ = product["price"] - salePrice;
    return console.log(`Початкова сума ${product['price']}. Cума до оплати з урахуванням знижки ${summ}`)
}
countSale()
//
//
//
//
// 2. Напишіть функцію, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,
// наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік
// за допомогою prompt, і викличте функцію з введеними даними. Результат виклику функції виведіть з допомогою alert.
//
let userName = prompt('Please enter your name :');
while (!(userName)){
    prompt('ERROR.Please enter your name :');
    break;
}

let userAge = +prompt('Please enter your age: ')
while (!(userAge)){
    +prompt(' ERROR. Please enter your age: ')
    break;
}
let userInfo = {
    name: userName,
    age: userAge
}

function sayHello() {
return alert (`Hello ${userInfo['name']} your age is : ${userInfo['age']}`)

}
sayHello(userInfo);



//
// 3.Опціональне. Завдання:
// Реалізувати повне клонування об'єкта.
let user ={
    name: 'Alex',
    city: 'New York',
    age: 25,
}
let userCopy ={};

for ( let key in user){
    userCopy[key] = user[key];
}
console.log(userCopy);

// Технічні вимоги:
//     - Написати функцію для рекурсивного повного клонування об'єкта (без єдиної передачі за посиланням, внутрішня вкладеність властивостей об'єкта може бути досить великою).
// - Функція має успішно копіювати властивості у вигляді об'єктів та масивів на будь-якому рівні вкладеності.
// - У коді не можна використовувати вбудовані механізми клонування, такі як функція Object.assign() або spread.

